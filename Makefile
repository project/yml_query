phony:ALL

## Run phpcs to validate the Drupal Code Standards.
validate:
	docker run --rm -v $(PWD):/tmp pathtoproject/phpcs-drupal:v1.5.1 phpcs --standard=Drupal --extensions=php,module,inc,install,test,profile,theme,css,info,txt,md,yml --ignore=*.md,node_modules,bower_components,vendor,*css .