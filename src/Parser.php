<?php

namespace Drupal\yml_query;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Parser service.
 *
 * Used to parse YAML into entity queries.
 */
class Parser {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a Parser object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Adds Dependency injection friendly access to Drupal::entityQuery.
   *
   * @param string $entity_type
   *   The entity type (for example, node) for which the query object should
   *   be returned.
   * @param string $conjunction
   *   (optional) Either 'AND' if all conditions in the query need to apply, or
   *   'OR' if any of them is sufficient. Defaults to 'AND'.
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   *   \Drupal\Core\Entity\Query\QueryInterface The query object that can query
   *   for the given entity type.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function entityQuery(string $entity_type, string $conjunction = 'AND') : QueryInterface {
    return $this->entityTypeManager->getStorage($entity_type)->getQuery($conjunction);
  }

  /**
   * Parse the yaml string using Entity Query.
   *
   * @param string $yml_query
   *   A string containing the YAML query.
   *
   *   This string should be a dictionary with at least a main key as the query.
   *   Other queries are optional and can be used to join or sub-query.
   *   Example:
   *   ---
   *   main:
   *     entity_type: node
   *     range:
   *     start: 0
   *     length: 10
   *     sort:
   *       field: nid
   *     conditions:
   *     - field: status
   *       value: published
   *       operator: "!="
   *     - field: nid
   *       value: condition_1
   *
   *   condition_1:
   *     entity_type: node
   *     range:
   *     start: 0
   *     length: 10
   *     sort:
   *     field: nid
   *     conditions:
   *     - field: nid
   *     value: 1
   *     operator: "="
   *     ...
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   *   A query to be executed.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function parse(string $yml_query) : QueryInterface {
    // Parse the YAML from $yml_query into a PHP array.
    $yaml = Yaml::parse($yml_query);
    $queries = [];

    foreach ($yaml as $query_id => $query_definition) {
      $queries[$query_id] = $this->entityQuery($query_definition['entity_type']);

      // Add the query conditions.
      foreach ($query_definition['conditions'] ?? [] as $condition_id => $condition_definition) {
        if (!in_array($condition_definition['value'], array_keys($yaml))) {
          $queries[$query_id]->condition($condition_definition['field'], $condition_definition['value'] ?? NULL, $condition_definition['operator'] ?? '=', $condition_definition['langcode'] ?? NULL);
        }
      }
      // Add the query sort.
      if (isset($query_definition['sort'])) {
        $queries[$query_id]->sort($query_definition['sort']['field'], $query_definition['sort']['direction'] ?? NULL, $query_definition['sort']['langcode'] ?? NULL);
      }
      // Add the query range.
      $queries[$query_id]->range($query_definition['range']['start'], $query_definition['range']['length']);
      // Add the query access check.
      $queries[$query_id]->accessCheck($query_definition['access_check'] ?? TRUE);

    }
    foreach ($yaml as $query_id => $query_definition) {
      // Go through the conditions a second time to add sub-query conditions.
      foreach ($query_definition['conditions'] ?? [] as $condition_id => $condition_definition) {
        if (in_array($condition_definition['value'], array_keys($yaml))) {
          $queries[$query_id]->condition($condition_definition['field'], $queries[$condition_definition['value']]->execute() ?? NULL, $condition_definition['operator'] ?? '=', $condition_definition['langcode'] ?? NULL);
        }
      }
    }

    return $queries['main'];
  }

}
