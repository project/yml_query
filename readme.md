This project provides a Service that can be used to parse YAML into an executable EntityQuery.

## Example Yaml
```yaml
---
main:
  entity_type: node
  range:
    start: 0
    length: 10
  sort:
    field: nid
  conditions:
      - field: status
        value: published
        operator: "!="
      - field: nid
        value: condition_1

condition_1:
  entity_type: node
  range:
    start: 0
    length: 10
  sort:
    field: nid
  conditions:
      - field: nid
        value: 1
        operator: "="
...
```

Running this through the Yaml Parser will return an EntityQuery which can be executed. Notice Queries can be linked to conditions as sub-queries.
